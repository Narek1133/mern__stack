First, run the development server:
```bash
$ cd ../mern__task
$ npm install

$ cd ../client
$ npm install

$ npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
