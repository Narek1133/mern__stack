import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import {HomePage} from './pages/HomePage';
import {SignInPage} from './pages/SignInPage';
import {SignUpPage} from './pages/SignUpPage';
import {GeneralNavbar} from './components/GeneralNavbar';

export const useRoutes = isAuthenticated => {
  if (isAuthenticated) {
    return (
      <Switch>
        <Route path="/home" exact>
          <HomePage />
        </Route>
        <Redirect to="/home" />
      </Switch>
    )
  }

  return (
    <Switch>
      <Route path="/" exact>
        <GeneralNavbar />
      </Route>
      <Route path="/signin" exact>
        <SignInPage />
      </Route>
      <Route path="/signup" exact>
        <SignUpPage />
      </Route>
      <Redirect to="/" />
    </Switch>
  )
}
