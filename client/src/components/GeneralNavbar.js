import React from 'react';
import {Link} from 'react-router-dom';

export const GeneralNavbar = () => {
  return (
    <div className="navbar-fixed">
      <nav>
        <div className="nav-wrapper pink lighten-2" style={{ padding: '0 2rem' }}>
          <span className="brand-logo"><Link to="/">Home Page</Link></span>
          <ul id="nav-mobile" className="right hide-on-med-and-down">
            <li><Link to="/signup">SignUp</Link></li>
          </ul>
          <ul id="nav-mobile" className="right hide-on-med-and-down">
            <li><Link to="/signin">SignIn</Link></li>
          </ul>
        </div>
      </nav>
    </div>
  );
};
