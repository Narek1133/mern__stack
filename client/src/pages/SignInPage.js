import React, {useContext, useEffect, useState} from 'react'
import {Link} from 'react-router-dom';
import {useHttp} from '../hooks/http.hook';
import {useMessage} from '../hooks/message.hook';
import {AuthContext} from '../context/AuthContext';
import {GeneralNavbar} from '../components/GeneralNavbar';

export const SignInPage = () => {
  const auth = useContext(AuthContext);
  const message = useMessage();
  const {loading, request, error, clearError} = useHttp();
  const [form, setForm] = useState({
    email: '', 
    password: ''
  });

  useEffect(() => {
    message(error);
    clearError();
  }, [error, message, clearError]);

  useEffect(() => {
    window.M.updateTextFields();
  }, []);

  const changeHandler = event => {
    setForm({ ...form, [event.target.name]: event.target.value });
  };
  
  const loginHandler = async () => {
    try {
      const data = await request('/api/auth/login', 'POST', {...form});
      auth.login(data.token, data.userId);
    } catch (e) {}
  };

  const pressLoginHandler = async (event) => {
    if (event.key === 'Enter') {
      try {
        const data = await request('/api/auth/login', 'POST', {...form});
        auth.login(data.token, data.userId);
      } catch (e) {}
    }
  }

  return (
    <div 
      className="row" 
      onKeyPress={pressLoginHandler}>
      <GeneralNavbar />
      <div className="col s6 offset-s3">
      <h4 style={{ margin: '13px 0' }}>SignIn</h4>
        <div className="card blue darken-1">
          <div className="card-content white-text">
            <span className="card-title">Authorization</span>
            <div>
              <div className="input-field">
                <input
                  placeholder="Enter your email"
                  id="email"
                  type="text"
                  name="email"
                  className="yellow-input"
                  value={form.email}
                  onChange={changeHandler}
                />
                <label htmlFor="email">Email</label>
              </div>

              <div className="input-field">
                <input
                  placeholder="Enter a password"
                  id="password"
                  type="password"
                  name="password"
                  className="yellow-input"
                  value={form.password}
                  onChange={changeHandler}
                />
                <label htmlFor="email">Password</label>
              </div>
            </div>
          </div>
          <div className="card-action">
            <button
              className="btn yellow darken-4"
              style={{marginRight: 10, marginTop: '-50px'}}
              disabled={loading}
              onClick={loginHandler}
            >
              Log In
            </button>
            <div>
              Don't have an account? <Link to="/signup" style={{color: "white"}}>SignUp</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
