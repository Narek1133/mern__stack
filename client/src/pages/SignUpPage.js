import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import {useHttp} from '../hooks/http.hook';
import {useMessage} from '../hooks/message.hook';
import {GeneralNavbar} from '../components/GeneralNavbar';

export const SignUpPage = () => {
  const message = useMessage();
  const history = useHistory();
  const {loading, request, error, clearError} = useHttp();
  const [form, setForm] = useState({
    first_name: '',
    last_name: '',
    email: '', 
    phone:'',
    password: '',  
  });

  useEffect(() => {
    message(error);
    clearError();
  }, [error, message, clearError]);

  useEffect(() => {
    window.M.updateTextFields();
  }, []);

  const changeHandler = event => {
    setForm({ ...form, [event.target.name]: event.target.value })
  };

  const registerHandler = async () => {
    try {
      const data = await request('/api/auth/register', 'POST', {...form});
      message(data.message);
      history.push('/signin');
    } catch (e) {}
  };

  const pressSignupHandler = async event => {
    if (event.key === 'Enter') {
      try {
        const data = await request('/api/auth/register', 'POST', {...form});
        message(data.message);
        history.push('/signin');
      } catch (e) {}
    }
  };

  return (
    <div 
      className="row"
      onKeyPress={pressSignupHandler}>
      <GeneralNavbar />
      <div className="col s6 offset-s3">
        <h4 style={{ margin: '13px 0' }}>SignUp</h4>
        <div className="card blue darken-1">
          <div className="card-content white-text">
            <span className="card-title">Authorization</span>
            <div className="input-field">
              <input
                  placeholder="Enter your first_name"
                  id="first_name"
                  type="text"
                  name="first_name"
                  className="yellow-input"
                  value={form.first_name}
                  onChange={changeHandler}
              />
              <label htmlFor="first_name">First_Name</label>
            </div>

            <div className="input-field">
              <input
                placeholder="Enter your last_name"
                id="last_name"
                type="text"
                name="last_name"
                className="yellow-input"
                value={form.last_name}
                onChange={changeHandler}
              />
              <label htmlFor="last_name">Last_Name</label>
            </div>

            <div>
              <div className="input-field">
                <input
                  placeholder="Enter your email"
                  id="email"
                  type="text"
                  name="email"
                  className="yellow-input"
                  value={form.email}
                  onChange={changeHandler}
                />
                <label htmlFor="email">Email</label>
              </div>

              <div className="input-field">
                <input
                  placeholder="Enter your phone-number"
                  id="phone"
                  type="text"
                  name="phone"
                  className="yellow-input"
                  value={form.phone}
                  onChange={changeHandler}
                />
                <label htmlFor="last_name">Phone</label>
              </div>

              <div className="input-field">
                <input
                  placeholder="Enter a password"
                  id="password"
                  type="password"
                  name="password"
                  className="yellow-input"
                  value={form.password}
                  onChange={changeHandler}
                />
                <label htmlFor="email">Пароль</label>
              </div>
            </div>
          </div>
          <div className="card-action">
            <button
              className="btn grey lighten-1 black-text"
              style={{ marginTop: '-50px'}}
              disabled={loading}
              onClick={registerHandler}
            >
              SignUp
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
